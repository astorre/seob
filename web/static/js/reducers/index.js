import { combineReducers }  from 'redux';
import { routerReducer }    from 'react-router-redux';
import session              from './session';
import registration         from './registration';
import tasks                from './tasks';
import currentTask          from './current_task';
import header               from './header';

export default combineReducers({
  routing: routerReducer,
  session: session,
  registration: registration,
  tasks: tasks,
  currentTask: currentTask,
  header: header,
});
