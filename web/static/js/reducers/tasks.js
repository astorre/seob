import Constants from '../constants';

const initialState = {
  ownedTasks: [],
  showForm: false,
  formErrors: null,
  fetching: true,
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case Constants.TASKS_FETCHING:
      return { ...state, fetching: true };

    case Constants.TASKS_RECEIVED:
      return { ...state, ownedTasks: action.ownedTasks, fetching: false };

    case Constants.TASKS_SHOW_FORM:
      return { ...state, showForm: action.show };

    case Constants.TASKS_CREATE_ERROR:
      return { ...state, formErrors: action.errors };

    case Constants.TASKS_NEW_TASK_CREATED:
      const { ownedTasks } = state;

      return { ...state, ownedTasks: [action.task].concat(ownedTasks) };

    default:
      return state;
  }
}
