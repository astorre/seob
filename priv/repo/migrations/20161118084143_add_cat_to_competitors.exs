defmodule Seob.Repo.Migrations.AddCatToCompetitors do
  use Ecto.Migration

  def change do
    alter table(:competitors) do
      add :cat, :integer
    end
  end
end
