defmodule Aparser do
  # import Floki

  @api_url "http://185.87.193.181:9091/API"
  @passwd "seointellect"

  def check_multiple_times(aparser_task_id, state) when state == "completed" do
    state
  end

  def check_multiple_times(aparser_task_id, state) do
    :timer.sleep(5000)
    state = get_task_state(aparser_task_id)
    IO.puts "#{aparser_task_id}: #{state["status"]}"
    check_multiple_times(aparser_task_id, state["status"])
  end

  # def one_request(parser, preset, query, opts \\ %{}) do
  #   margs = %{
  #     parser: parser,
  #     preset: preset,
  #     query: query,
  #     rawResults: 1
  #   }
  #   do_request("oneRequest", margs, opts)
  # end

  def add_task(config_preset, preset, queries_from, queries, opts \\ %{}) do
    margs = %{
      configPreset: config_preset,
      preset: preset,
      queriesFrom: queries_from,
      queries: queries
    }
    do_request("addTask", margs, opts)
  end

  def get_task_state(task_id) do
    margs = %{
      taskUid: task_id
    }
    do_request("getTaskState", margs)
  end

  def get_task_conf(task_id) do
    margs = %{
      taskUid: task_id
    }
    do_request("getTaskConf", margs)
  end

  # starting|pausing|stopping|deleting
  def change_task_status(task_id, to_status) do
    margs = %{
      taskUid: task_id,
      toStatus: to_status
    }
    do_request("getTaskState", margs)
  end

  def get_task_results_file(task_id) do
    margs = %{
      taskUid: task_id
    }
    do_request("getTaskResultsFile", margs)
  end

  def open_file(file_path) do
    case HTTPoison.get(file_path) do
      {:ok, %HTTPoison.Response{status_code: 200, body: body}} ->
        {:ok, body}
      {:ok, %HTTPoison.Response{status_code: 400}} ->
        IO.puts "Bad Request :("
        {:ok, []}
      {:ok, %HTTPoison.Response{status_code: 404}} ->
        IO.puts "Not found :("
        {:ok, []}
      {:error, %HTTPoison.Error{reason: reason}} ->
        IO.inspect "Error: #{reason}"
        {:error, nil}
    end
  end

  defp response(request, payload) do
    case HTTPoison.post(request, payload, [{"Content-Type", "text/plain; charset=UTF-8"}]) do
      {:ok, %HTTPoison.Response{status_code: 200, body: body}} ->
        {:ok, body}
      {:ok, %HTTPoison.Response{status_code: 400}} ->
        IO.puts "Bad Request :("
        {:ok, []}
      {:ok, %HTTPoison.Response{status_code: 404}} ->
        IO.puts "Not found :("
        {:ok, []}
      {:error, %HTTPoison.Error{reason: reason}} ->
        IO.inspect "Error: #{reason}"
        {:error, nil}
    end
  end

  defp do_request(action, data \\ nil, opts \\ %{}) do

    payload =
      case data do
        nil ->
          IO.puts "Data is nil"
          %{
            password: @passwd,
            action: action
          }
        _ ->
          %{
            password: @passwd,
            action: action,
            data: Map.merge(data, opts)
          }
      end

    {_, res} = response(@api_url, Poison.encode!(payload))

    response = if res == nil, do: "Empty response !", else: Poison.decode!(res)

    if res != nil && Map.has_key?(response, "success") do
      response["data"]
    else
      nil
    end
  end
end
