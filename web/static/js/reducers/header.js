import Constants from '../constants';

const initialState = {
  showTasks: false,
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case Constants.HEADER_SHOW_TASKS:
      return { ...state, showTasks: action.show };

    default:
      return state;
  }
}
