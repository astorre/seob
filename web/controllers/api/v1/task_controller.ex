defmodule Seob.TaskController do
  use Seob.Web, :controller
  require Logger

  plug Guardian.Plug.EnsureAuthenticated, handler: Seob.SessionController
  plug :scrub_params, "task" when action in [:create]

  alias Seob.{Task}

  def index(conn, _params) do
    current_user = Guardian.Plug.current_resource(conn)

    owned_tasks = current_user
      |> assoc(:owned_tasks)
      |> Task.preload_all
      |> Repo.all

    render(conn, "index.json", owned_tasks: owned_tasks)
  end

  def create(conn, %{"task" => task_params}) do
    current_user = Guardian.Plug.current_resource(conn)

    # perform_at = Timex.shift(Timex.DateTime.now, seconds: 30)
    # Verk.schedule(%Verk.Job{queue: :default, class: "ExampleWorker", args: [1,2]}, perform_at)

    changeset = current_user
      |> build_assoc(:owned_tasks)
      |> Task.changeset(task_params)

    case Repo.insert(changeset) do
      {:ok, task} ->
        urls = task_params["urls"]
        # Logger.debug "URLs: #{inspect(urls)}"
        # Logger.debug "id: #{inspect(task.id)}"
        Verk.enqueue(%Verk.Job{queue: :default, class: "ExampleWorker", args: [task.id, urls]})
        conn
        |> put_status(:created)
        |> render("show.json", task: task)
      {:error, changeset} ->
        conn
        |> put_status(:unprocessable_entity)
        |> render("error.json", changeset: changeset)
    end
  end
end
