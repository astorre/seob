defmodule Seob.Competitor do
  use Seob.Web, :model

  alias Seob.{Task, Repo}

  @derive {Poison.Encoder, only: [:id,
                                  :url,
                                  :position,
                                  :tic,
                                  :cat,
                                  :dmoz,
                                  :y,
                                  :g,
                                  :bc,
                                  :ubc,
                                  :age,
                                  :query_quant,
                                  :soc,
                                  :ip,
                                  :ip_sites]}

  schema "competitors" do
    field :url, :string
    field :position, :integer
    field :tic, :integer
    field :cat, :integer
    field :dmoz, :integer
    field :y, :integer
    field :g, :integer
    field :bc, :integer
    field :ubc, :integer
    field :age, Ecto.DateTime
    field :query_quant, :integer
    field :soc, :integer
    field :ip, :string
    field :ip_sites, :integer

    belongs_to :task, Task

    timestamps()
  end

  @required_fields ~w(url)
  @optional_fields ~w(position tic cat dmoz y g bc ubc age query_quant soc ip ip_sites)

  @doc """
  Creates a changeset based on the `model` and `params`.

  If no params are provided, an invalid changeset is returned
  with no validation performed.
  """
  def changeset(model, params \\ %{}) do
    model
    |> cast(params, @required_fields, @optional_fields)
  end
end
