defmodule Seob.Repo.Migrations.AddRestFieldsToCompetitors do
  use Ecto.Migration

  def change do
    alter table(:competitors) do
      add :y, :integer
      add :g, :integer
      add :bc, :integer
      add :ubc, :integer
      add :age, :datetime
      add :query_quant, :integer
      add :soc, :integer
      add :ip, :string
      add :ip_sites, :integer
    end
  end
end
