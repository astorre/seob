import React, { PropTypes } from 'react';
import {ReactPageClick}     from 'react-page-click';
import Actions              from '../../actions/tasks';
import {renderErrorsFor}    from '../../utils';

export default class TaskForm extends React.Component {
  componentDidMount() {
    this.refs.urls.focus();
  }

  _handleSubmit(e) {
    e.preventDefault();

    const { dispatch } = this.props;
    const { urls, site } = this.refs;

    const data = {
      urls: urls.value,
      site: site.value,
    };

    dispatch(Actions.create(data));
  }

  _handleCancelClick(e) {
    e.preventDefault();

    this.props.onCancelClick();
  }

  render() {
    const { errors } = this.props;

    return (
      <ReactPageClick notify={::this._handleCancelClick}>
        <div className="task form">
          <div className="inner">
            <h4>Новая задача</h4>
            <form id="new_task_form" onSubmit={::this._handleSubmit}>
              <textarea rows="10" cols="90" ref="urls" id="task_urls" placeholder="Конкуренты..." required="true"/>
              {renderErrorsFor(errors, 'urls')}
              <input ref="site" id="task_site" type="text" placeholder="Мой сайт..." required="true"/>
              {renderErrorsFor(errors, 'site')}
              <button type="submit">Запустить</button> или <a ref="#" onClick={::this._handleCancelClick}>отменить</a>
            </form>
          </div>
        </div>
      </ReactPageClick>
    );
  }
}
