defmodule Seob.CompetitorTest do
  use Seob.ModelCase

  alias Seob.Competitor

  @valid_attrs %{url: "some content"}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = Competitor.changeset(%Competitor{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = Competitor.changeset(%Competitor{}, @invalid_attrs)
    refute changeset.valid?
  end
end
