defmodule Seob.PageController do
  use Seob.Web, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end
end
