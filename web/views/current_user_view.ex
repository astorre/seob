defmodule Seob.CurrentUserView do
  use Seob.Web, :view

  def render("show.json", %{user: user}) do
    user
  end

  def render("error.json", _) do
  end
end
