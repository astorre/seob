# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# Configures the endpoint
config :seob, Seob.Endpoint,
  url: [host: "localhost"],
  root: Path.dirname(__DIR__),
  render_errors: [accepts: ~w(html json)],
  pubsub: [name: Seob.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"

# Configure phoenix generators
config :phoenix, :generators,
  migration: true,
  binary_id: false

config :guardian, Guardian,
  allowed_algos: ["ES512"],
  issuer: "Seob",
  ttl: { 3, :days },
  verify_issuer: true,
  secret_key: %{
    "alg" => "ES512",
    "crv" => "P-521",
    "d" => "Aau0pRcBVGDg3_EJ-kD8CEf_44cUM2QOWRiOV81EScis_mNJyvrgNEf6XAZdbJ7o2NbQ45minzjm7zHSdmx3fZnl",
    "kty" => "EC", "use" => "sig",
    "x" => "Ae_qfGtBy5cylNhK6-cYHfRqj5yEg7giRQgRERsNzqP711vfj6wGKxAGvgkpPxZEkQDjCzNou_ij3OO3PD1CneSi",
    "y" => "AR5YqEsCu7q0T9jDXFn3i0IF1LFczZAXYqGRVFsTP8xriMaK1YnOtA4JrsUWMgp7CcwmtI48Y8k-K5NyIEHaHOVv"
  },
  serializer: Seob.GuardianSerializer

config :seob, :ecto_repos, [Seob.Repo]
