import Constants              from '../constants';
import { push }               from 'react-router-redux';
import CurrentTaskActions     from './current_task';

const Actions = {
  showTasks: (show) => {
    return dispatch => {
      dispatch({
        type: Constants.HEADER_SHOW_TASKS,
        show: show,
      });
    };
  },

  visitTask: (socket, channel, taskId) => {
    return dispatch => {
      if (channel) {
        dispatch(CurrentTaskActions.leaveChannel(channel));
        dispatch(CurrentTaskActions.connectToChannel(socket, taskId));
      }

      dispatch(push(`/tasks/${taskId}`));

      dispatch({
        type: Constants.HEADER_SHOW_TASKS,
        show: false,
      });
    };
  },
};

export default Actions;
