import React            from 'react';
import { connect }      from 'react-redux';
import TasksActions     from '../actions/tasks';
import Header           from '../layouts/header';

class AuthenticatedContainer extends React.Component {
  componentDidMount() {
    const { dispatch } = this.props;
    dispatch(TasksActions.fetchTasks());
  }

  render() {
    const { currentUser, dispatch, tasks, socket, currentTask } = this.props;

    if (!currentUser) return false;

    return (
      <div id="authentication_container" className="application-container">
        <Header/>

        <div className="main-container">
          {this.props.children}
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  currentUser: state.session.currentUser,
  socket: state.session.socket,
  channel: state.session.channel,
  tasks: state.tasks,
  currentTask: state.currentTask,
});

export default connect(mapStateToProps)(AuthenticatedContainer);
