import React, {PropTypes} from 'react';
import { push }           from 'react-router-redux';

export default class TaskCard extends React.Component {
  _handleClick() {
    this.props.dispatch(push(`/tasks/${this.props.id}`));
  }

  render() {
    return (
      <div id={this.props.id} className="task" onClick={::this._handleClick}>
        <div className="inner">
          <h4>Задача №{this.props.id}</h4>
          <p>{this.props.type}</p>
          <p>{this.props.region}</p>
          <p>{this.props.site}</p>
        </div>
      </div>
    );
  }
}

TaskCard.propTypes = {

};
