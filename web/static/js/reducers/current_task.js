import Constants from '../constants';

const initialState = {
  channel: null,
  fetching: true,
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case Constants.CURRENT_TASK_FETCHING:
      return { ...state, fetching: true };

    case Constants.TASKS_SET_CURRENT_TASK:
      return { ...state, fetching: false, ...action.task };

    case Constants.CURRENT_TASK_CONNECTED_TO_CHANNEL:
      return { ...state, channel: action.channel };

    default:
      return state;
  }
}
