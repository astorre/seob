import React                from 'react';
import { connect }          from 'react-redux';
import classnames           from 'classnames';

import { setDocumentTitle } from '../../utils';
import Actions              from '../../actions/tasks';
import TaskCard             from '../../components/tasks/card';
import TaskForm             from '../../components/tasks/form';

class HomeIndexView extends React.Component {
  componentDidMount() {
    setDocumentTitle('Задачи');
  }

  _renderOwnedTasks() {
    const { fetching } = this.props;

    let content = false;

    const iconClasses = classnames({
      fa: true,
      'fa-user': !fetching,
      'fa-spinner': fetching,
      'fa-spin': fetching,
    });

    if (!fetching) {
      content = (
        <div className="tasks-wrapper">
          {::this._renderAddNewTask()}
          {::this._renderTasks(this.props.ownedTasks)}
        </div>
      );
    }

    return (
      <section>
        <header className="view-header">
          <h3><i className={iconClasses} /> Мои задачи</h3>
        </header>
        {content}
      </section>
    );
  }

  _renderTasks(tasks) {
    return tasks.map((task) => {
      return <TaskCard
                key={task.id}
                dispatch={this.props.dispatch}
                {...task} />;
    });
  }

  _renderAddNewTask() {
    let { showForm, dispatch, formErrors } = this.props;

    if (!showForm) return this._renderAddButton();

    return (
      <TaskForm
        dispatch={dispatch}
        errors={formErrors}
        onCancelClick={::this._handleCancelClick} />
    );
  }

  _renderAddButton() {
    return (
      <div className="task add-new" onClick={::this._handleAddNewClick}>
        <div className="inner">
          <a id="add_new_task">Добавить новую задачу...</a>
        </div>
      </div>
    );
  }

  _handleAddNewClick() {
    let { dispatch } = this.props;

    dispatch(Actions.showForm(true));
  }

  _handleCancelClick() {
    this.props.dispatch(Actions.showForm(false));
  }

  render() {
    return (
      <div className="view-container tasks index">
        {::this._renderOwnedTasks()}
      </div>
    );
  }
}

const mapStateToProps = (state) => (
  state.tasks
);

export default connect(mapStateToProps)(HomeIndexView);
