defmodule Seob.Repo.Migrations.AddTicToCompetitors do
  use Ecto.Migration

  def change do
    alter table(:competitors) do
      add :tic, :integer
    end
  end
end
