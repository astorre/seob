defmodule Seob.Repo.Migrations.AddDmozToCompetitors do
  use Ecto.Migration

  def change do
    alter table(:competitors) do
      add :dmoz, :integer
    end
  end
end
