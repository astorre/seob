defmodule Seob.TaskChannel do
  @moduledoc """
  Task channel
  """

  use Seob.Web, :channel

  alias Seob.Task
  alias Seob.TaskChannel.Monitor

  def join("tasks:" <> task_id, _params, socket) do
    current_user = socket.assigns.current_user
    task = get_current_task(socket, task_id)

    Monitor.create(task_id)

    send(self, {:after_join, Monitor.user_joined(task_id, current_user.id)})

    {:ok, %{task: task}, assign(socket, :task, task)}
  end

  def handle_info({:after_join, connected_users}, socket) do
    broadcast! socket, "user:joined", %{users: connected_users}
    {:noreply, socket}
  end

  def terminate(_reason, socket) do
    task_id = socket.assigns.task.id
    user_id = socket.assigns.current_user.id

    broadcast! socket, "user:left", %{users: Monitor.user_left(task_id, user_id)}

    :ok
  end

  defp get_current_task(socket, task_id) do
    socket.assigns.current_user
    |> assoc(:owned_tasks)
    |> Task.preload_all
    |> Repo.get(task_id)
  end

  defp get_current_task(socket), do: get_current_task(socket, socket.assigns.task.id)
end
