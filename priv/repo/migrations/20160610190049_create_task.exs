defmodule Seob.Repo.Migrations.CreateTask do
  use Ecto.Migration

  def change do
    create table(:tasks) do
      add :site, :string, null: false
      add :user_id, references(:users, on_delete: :delete_all), null: false

      timestamps
    end
    create index(:tasks, [:user_id])

  end
end
