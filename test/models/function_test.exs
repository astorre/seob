defmodule Seob.FunctionTest do
  use Seob.ModelCase

  alias Seob.Function

  @valid_attrs %{region: 42, site: "some content", type: 42}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = Function.changeset(%Function{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = Function.changeset(%Function{}, @invalid_attrs)
    refute changeset.valid?
  end
end
