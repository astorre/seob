import React, {PropTypes}   from 'react';
import { connect }          from 'react-redux';

import Actions              from '../../actions/current_task';
import Constants            from '../../constants';
import { setDocumentTitle } from '../../utils';
// import TaskMembers          from '../../components/tasks/members';
import JsonTable            from 'react-json-table';

class TasksShowView extends React.Component {
  componentDidMount() {
    const { socket } = this.props;

    if (!socket) {
      return false;
    }

    this.props.dispatch(Actions.connectToChannel(socket, this.props.params.id));
  }

  componentWillUnmount() {
    this.props.dispatch(Actions.leaveChannel(this.props.currentTask.channel));
  }

  _renderTable() {
    const { competitors, channel, id } = this.props.currentTask;
    // let items = competitors.map((competitor) => {
    //   { id: competitor.id, position: competitor.position, task_id: competitor.task_id, url: competitor.url }
    // });
    // let items = [
    //   { name: 'Louise', age: 27, color: 'red' },
    //   { name: 'Margaret', age: 15, color: 'blue'},
    //   { name: 'Lisa', age:34, color: 'yellow'}
    // ]

    return (
      <JsonTable rows={competitors} className="table" />
    );
  }

  render() {
    const { fetching, id, site } = this.props.currentTask;

    if (fetching) return (
      <div className="view-container tasks show">
        <i className="fa fa-spinner fa-spin"/>
      </div>
    );

    return (
      <div className="view-container tasks show">
        <header className="view-header">
          <h3>Site: {site} (id={id})</h3>
        </header>
        <div className="canvas-wrapper">
          <div className="canvas">
            {::this._renderTable()}
          </div>
        </div>
        {this.props.children}
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  currentTask: state.currentTask,
  socket: state.session.socket,
  currentUser: state.session.currentUser,
});

export default connect(mapStateToProps)(TasksShowView);
