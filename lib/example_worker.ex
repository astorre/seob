defmodule ExampleWorker do
  def perform(task_id, urls) do

    alias YandexXml
    alias Aparser

    urls = String.split(urls, "\n")

    task_list = [{"default", "default", "text", urls, %{ parsers: [["Rank::Linkpad", "default"]] }},
                 {"default", "default", "text", urls |> Enum.map(fn e -> "site:#{e}" end), %{ parsers: [["SE::Google", "Google-parser"]] }}]
    linkpad_hashes | google_hashes = add_tasks(task_list)

    # pars_opt = %{ parsers: [["Rank::Linkpad", "default"]] }
    # aparser_task_id = Aparser.add_task("default", "default", "text", urls, pars_opt)
    #                   |> String.to_integer
    # aparser_task_id = Aparser.add_task("default", "default", "text", ["сериал викинги"],
    #                            %{ parsers: [["SE::Yandex::WordStat",
    #                                         "With_captcha",
    #                                         %{ type: "options",
    #                                            id: "parseLevel",
    #                                            value: 0,
    #                                            additional: %{ parseLevelTreshold: 10 } }]],
    #                               options: [%{ value: 1, type: "override", id: "pagecount" },
    #                                         %{ value: [213], type: "override", id: "geo" },
    #                                         %{ value: true, type: "override", id: "removeplus" },
    #                                         %{ value: true, type: "override", id: "useproxy" },
    #                                         %{ value: "Rucaptcher", type: "override", id: "antigatepreset" }] })
    #                   |> String.to_integer
    # IO.puts "TASK: #{aparser_task_id}"

    # if aparser_task_id != nil do

    #   status = Aparser.check_multiple_times(aparser_task_id, "waitSlot")

    #   IO.puts "Task state: #{status}"
    #   IO.puts "File name: #{Aparser.get_task_conf(aparser_task_id)["resultsFileName"]}"
    #   file_path = Aparser.get_task_results_file(aparser_task_id)
    #   IO.inspect Aparser.change_task_status(aparser_task_id, "deleting")

    #   {_, response} = Aparser.open_file(file_path)

    #   IO.inspect response

    #   donors_hasher = fn x ->
    #     splitter = String.split(x, " - donors: ")
    #     {splitter |> Enum.at(0), Regex.run(~r/\d+/, splitter |> Enum.at(1))}
    #   end

    #   hashes = response
    #            |> String.split("\n")
    #            |> Enum.filter_map(fn(str) -> String.contains?(str, "donors: ") end, &donors_hasher.(&1))

    #   IO.inspect hashes
    # else
    #   IO.puts "ERROR #{aparser_task_id}"
    # end

    cur_date = Ecto.DateTime.utc
    competitors = Enum.map(urls, fn(url) ->
      {_status, tic}         = YandexXml.tic(url)
      {_status, cat}         = YandexXml.cat(url)
      {_status, dmoz}        = YandexXml.dmoz(url)
      {_status, y}           = YandexXml.y(url)
      {_status, g}           = YandexXml.g(url)
      {_status, ubc}         = YandexXml.ubc(url)
      {_status, age}         = YandexXml.age(url)
      {_status, query_quant} = YandexXml.query_quant(url)
      {_status, soc}         = YandexXml.soc(url)
      {_status, ip}          = YandexXml.ip(url)
      {_status, ip_sites}    = YandexXml.ip_sites(url)

      # if List.keymember?(hashes, url, 0) do
      #   {h, rest} = List.keytake(hashes, url, 0)
      # end

      my_bc =
        case List.keytake(linkpad_hashes, url, 0) do
          {{_, nil}, _} ->
            0
          {{_, v}, _} ->
            v |> hd |> String.to_integer
        end

      g =
        case List.keytake(google_hashes, url, 0) do
          {{_, nil}, _} ->
            0
          {{_, v}, _} ->
            v |> hd |> String.to_integer
        end

      %{position: 1,
        url: url,
        tic: tic,
        cat: cat,
        dmoz: dmoz,
        y: y,
        g: g,
        bc: my_bc,
        ubc: ubc,
        age: age,
        query_quant: query_quant,
        soc: soc,
        ip: ip,
        ip_sites: ip_sites,
        task_id: task_id,
        inserted_at: cur_date,
        updated_at: cur_date} end)
    Seob.Repo.insert_all Seob.Competitor, competitors

    # Try to use this for long term save instead of partialy save to DB
    # table = :ets.new(:buckets_registry, [:set, :protected])
    # :ets.insert(table, {"foo", self})
    # IO.puts :ets.lookup(table, "foo")
  end

  defp add_tasks(task_list) do
    Enum.map(task_list, fn(task) ->
      {config_preset, preset, queries_from, queries, opts} = task
      %{ parsers: [[parser, parser_preset]] } = opts
      aparser_task_id = Aparser.add_task(config_preset, preset, queries_from, queries, opts)
                        |> String.to_integer

      case aparser_task_id do
        nil ->
          IO.puts "ERROR #{aparser_task_id}"
          []
        _ ->
          status = Aparser.check_multiple_times(aparser_task_id, "waitSlot")

          IO.puts "Task state: #{status}"
          IO.puts "File name: #{Aparser.get_task_conf(aparser_task_id)["resultsFileName"]}"
          file_path = Aparser.get_task_results_file(aparser_task_id)
          IO.inspect Aparser.change_task_status(aparser_task_id, "deleting")

          {_, response} = Aparser.open_file(file_path)

          IO.inspect response

          hashes =
            case parser do
              "Rank::Linkpad" -> handled_hashes(:linkpad)
              "SE::Google" -> handled_hashes(:google)
            end

          IO.inspect hashes
          hashes
      end
    end
  end

  defp handled_hashes(par, response) do
    case par do
      :linkpad ->
        linkpad_data(response)
      :google ->
        google_data(response)
    end

  end

  defp linkpad_data(response) do
    donors_hasher = fn x ->
      splitter = String.split(x, " - donors: ")
      {splitter |> Enum.at(0), Regex.run(~r/\d+/, splitter |> Enum.at(1))}
    end

    response
    |> String.split("\n")
    |> Enum.filter_map(fn(str) -> String.contains?(str, "donors: ") end, &donors_hasher.(&1))
  end

  defp google_data(response) do
  end
end
