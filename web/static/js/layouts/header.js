import React            from 'react';
import { connect }      from 'react-redux';
import { Link }         from 'react-router';
import {ReactPageClick} from 'react-page-click';
import { push }         from 'react-router-redux';

import SessionActions   from '../actions/sessions';
import HeaderActions    from '../actions/header';

class Header extends React.Component {
  _handleTasksClick(e) {
    e.preventDefault();

    const { dispatch } = this.props;
    const { ownedTasks } = this.props.tasks;

    if (ownedTasks.length != 0) {
      dispatch(HeaderActions.showTasks(true));
    } else {
      dispatch(push('/'));
    }
  }

  _renderTasks() {
    const { dispatch, currentTask, socket, header } = this.props;

    if (!header.showTasks) return false;

    const { ownedTasks } = this.props.tasks;

    const ownedTasksItems = ownedTasks.map((task) => {
      return this._createTaskItem(dispatch, currentTask, socket, task);
    });

    const ownedTasksItemsHeader = ownedTasksItems.length > 0 ? <header className="title"><i className="fa fa-user"/> Мои задачи</header> : null;

    return (
      <ReactPageClick notify={::this._hideTasks}>
        <div className="dropdown">
          {ownedTasksItemsHeader}
          <ul>
            {ownedTasksItems}
          </ul>
          <ul className="options">
            <li>
              <Link to="/" onClick={::this._hideTasks}>Все задачи</Link>
            </li>
          </ul>
        </div>
      </ReactPageClick>
    );
  }

  _hideTasks() {
    const { dispatch } = this.props;
    dispatch(HeaderActions.showTasks(false));
  }

  _createTaskItem(dispatch, currentTask, socket, task) {
    const onClick = (e) => {
      e.preventDefault();

      if (currentTask.id != undefined && currentTask.id == task.id) {
        dispatch(HeaderActions.showTasks(false));
        return false;
      }

      dispatch(HeaderActions.visitTask(socket, currentTask.channel, task.id));
    };

    return (
      <li key={task.id}>
        <a href="#" onClick={onClick}>{task.id}</a>
      </li>
    );
  }

  _renderCurrentUser() {
    const { currentUser } = this.props;

    if (!currentUser) {
      return false;
    }

    return (
      <a className="current-user">{currentUser.email}</a>
    );
  }

  _renderSignOutLink() {
    if (!this.props.currentUser) {
      return false;
    }

    return (
      <a href="#" onClick={::this._handleSignOutClick}><i className="fa fa-sign-out"/> Выйти</a>
    );
  }

  _handleSignOutClick(e) {
    e.preventDefault();

    this.props.dispatch(SessionActions.signOut());
  }

  render() {
    return (
      <header className="main-header">
        <nav id="tasks_nav">
          <ul>
            <li>
              <a className="tasks-button" href="#" onClick={::this._handleTasksClick}><i className="fa fa-columns"/> Задачи</a>
              {::this._renderTasks()}
            </li>
            <li>
              <a href="/verk" target="_blank"><i className="fa fa-cogs"/> Verk</a>
            </li>
          </ul>
        </nav>
        <Link to='/'>
          <span className='logo'/>
        </Link>
        <nav className="right">
          <ul>
            <li>
              {this._renderCurrentUser()}
            </li>
            <li>
              {this._renderSignOutLink()}
            </li>
          </ul>
        </nav>
      </header>
    );
  }
}

const mapStateToProps = (state) => ({
  currentUser: state.session.currentUser,
  socket: state.session.socket,
  tasks: state.tasks,
  currentTask: state.currentTask,
  header: state.header,
});

export default connect(mapStateToProps)(Header);
