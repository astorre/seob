import Constants from '../constants';

const Actions = {
  connectToChannel: (socket, taskId) => {
    return dispatch => {
      const channel = socket.channel(`tasks:${taskId}`);

      dispatch({ type: Constants.CURRENT_TASK_FETCHING });

      if (channel.state != 'joined') {
        console.log("tasks_channel_state: " + channel.state);
        channel.join().receive('ok', (response) => {
          dispatch({
            type: Constants.TASKS_SET_CURRENT_TASK,
            task: response.task,
          });

          dispatch({
            type: Constants.CURRENT_TASK_CONNECTED_TO_CHANNEL,
            channel: channel,
          });
        });
      }
    };
  },

  leaveChannel: (channel) => {
    return dispatch => {
      channel.leave();

      dispatch({
        type: Constants.CURRENT_TASK_RESET,
      });
    };
  },
};

export default Actions;
