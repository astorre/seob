defmodule Seob.TaskChannel.Monitor do
  @moduledoc """
  Board monitor that keeps track of connected users.
  """

  use GenServer

  #####
  # External API

  def create(task_id) do
    case GenServer.whereis(ref(task_id)) do
      nil ->
        Supervisor.start_child(Seob.TaskChannel.Supervisor, [task_id])
      _task ->
        {:error, :task_already_exists}
    end
  end

  def start_link(task_id) do
    GenServer.start_link(__MODULE__, [], name: ref(task_id))
  end

  def user_joined(task_id, user) do
    try_call task_id, {:user_joined, user}
  end

  def users_in_task(task_id) do
    try_call task_id, {:users_in_task}
  end

  def user_left(task_id, user) do
    try_call task_id, {:user_left, user}
  end

  #####
  # GenServer implementation

  def handle_call({:user_joined, user}, _from, users) do
    users = [user] ++ users
      |> Enum.uniq

    {:reply, users, users}
  end

  def handle_call({:users_in_task}, _from, users) do
    {:reply, users, users}
  end

  def handle_call({:user_left, user}, _from, users) do
    users = List.delete(users, user)

    {:reply, users, users}
  end

  defp ref(task_id) do
    {:global, {:task, task_id}}
  end

  defp try_call(task_id, call_function) do
    # IO.puts GenServer.whereis(ref(task_id))
    case GenServer.whereis(ref(task_id)) do
      nil ->
        "invalid_task"
      task ->
        GenServer.call(task, call_function)
    end
  end
end
