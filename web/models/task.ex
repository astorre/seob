defmodule Seob.Task do
  use Seob.Web, :model

  alias Seob.{Permalink, Competitor, User}

  @primary_key {:id, Permalink, autogenerate: true}

  schema "tasks" do
    field :site, :string

    belongs_to :user, User
    has_many :competitors, Competitor

    timestamps
  end

  @required_fields ~w(site)
  @optional_fields ~w()

  @doc """
  Creates a changeset based on the `model` and `params`.

  If no params are provided, an invalid changeset is returned
  with no validation performed.
  """
  def changeset(model, params \\ :empty) do
    model
    |> cast(params, @required_fields, @optional_fields)
  end

  def preload_all(query) do
    competitors_query = from c in Competitor, order_by: c.id, preload: :task

    from t in query, preload: [:user, competitors: ^competitors_query]
  end
end

defimpl Poison.Encoder, for: Seob.Task do
  def encode(model, options) do
    model
    |> Map.take([:id, :competitors, :site, :user])
    |> Poison.Encoder.encode(options)
  end
end
