defmodule Seob.TaskView do
  use Seob.Web, :view
  alias Seob.Task

  def render("index.json", %{owned_tasks: owned_tasks}) do
    %{owned_tasks: owned_tasks}
  end

  def render("show.json", %{task: task}) do
    %{
      id: task.id,
      site: task.site,
      user_id: task.user_id
    }
  end

  def render("error.json", %{changeset: changeset}) do
    errors = Enum.map(changeset.errors, fn {field, detail} ->
      %{} |> Map.put(field, detail)
    end)

    %{ errors: errors }
  end
end
