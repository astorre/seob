defmodule YandexXml do
  import Floki

  # def get_top do
  #   request = "https://yandex.ru/search/xml?user=michgan13&key=03.139220390:dbddc34fdb0b0349b9d28a846d820195&query=%D0%BF%D1%80%D0%BE%D0%B4%D0%B2%D0%B8%D0%B6%D0%B5%D0%BD%D0%B8%D0%B5+%D1%81%D0%B0%D0%B9%D1%82%D0%BE%D0%B2&lr=213&l10n=ru&sortby=rlv&filter=strict&groupby=attr%3Dd.mode%3Ddeep.groups-on-page%3D100.docs-in-group%3D1"

  #   case HTTPoison.get(request) do
  #     {:ok, %HTTPoison.Response{status_code: 200, body: body}} ->
  #       # IO.puts body
  #       result = body |> xpath(~x"//group//url/text()"l)
  #       IO.puts result
  #       {:ok, result}
  #     {:ok, %HTTPoison.Response{status_code: 404}} ->
  #       IO.puts "Not found :("
  #       {:ok, []}
  #     {:error, %HTTPoison.Error{reason: reason}} ->
  #       IO.inspect reason
  #       {:error, nil}
  #   end
  # end

  def tic(domain) do
    request = "https://yandex.ru/search/xml?user=#{System.get_env("YAUSER")}&key=#{System.get_env("YAKEY")}&query=site:#{domain}&lr=213&l10n=ru&sortby=rlv&filter=strict&groupby=attr%3Dd.mode%3Ddeep.groups-on-page%3D100.docs-in-group%3D1"
    response(request, "found[priority='all']")
  end

  def cat(domain) do
    request = "http://bar-navig.yandex.ru/u?ver=2&url=http://#{domain}&show=1"
    response(request, "textinfo")
  end

  def dmoz(domain) do
    request = "http://xml.alexa.com/data?cli=10&dat=nsa&ver=quirk-searchstatus&url=#{domain}"
    response(request, "alexa dmoz")
  end

  def y(domain) do
    case Regex.match?(~r/www./, domain) do
      true ->
        host = String.replace(domain, ~r/www./, "", global: false) # Only first occurrence
        www_host = domain
      false ->
        host = domain
        www_host = "www.#{domain}"
    end
    request = "https://yandex.ru/search/xml?user=#{System.get_env("YAUSER")}&key=#{System.get_env("YAKEY")}&query=url%3A#{host}*+%26+url%3A#{www_host}*&lr=213&l10n=ru&sortby=rlv&filter=strict&groupby=attr%3Dd.mode%3Ddeep.groups-on-page%3D100.docs-in-group%3D1"
    response(request, "found[priority='all']")
  end

  def g(domain) do
    {:error, nil}
  end

  # def bc(domain) do
  #   {:error, nil}
  # end

  def ubc(domain) do
    {:error, nil}
  end

  def age(domain) do
    {:error, Ecto.DateTime.utc}
  end

  def query_quant(domain) do
    {:error, nil}
  end

  def soc(domain) do
    {:error, nil}
  end

  def ip(domain) do
    {:error, nil}
  end

  def ip_sites(domain) do
    {:error, nil}
  end


  defp response(request, xml_path) do
    case HTTPoison.get(request) do
      {:ok, %HTTPoison.Response{status_code: 200, body: body}} ->
        res =
          case xml_path do
            "found[priority='all']" ->
              tic_parsed(body, xml_path)
            "textinfo" ->
              cat_parsed(body, xml_path)
            "alexa dmoz" ->
              dmoz_parsed(body, xml_path)
            _ ->
              nil
          end
        {:ok, res}
      {:ok, %HTTPoison.Response{status_code: 400}} ->
        IO.puts "Bad Request :("
        {:ok, []}
      {:ok, %HTTPoison.Response{status_code: 404}} ->
        IO.puts "Not found :("
        {:ok, []}
      {:error, %HTTPoison.Error{reason: reason}} ->
        IO.inspect "Error: #{reason}"
        {:error, nil}
    end
  end

  defp tic_parsed(body, xml_path) do
    doc = Floki.find(body, xml_path)
    case List.first(doc) do
      {tag, attrs, val} ->
        List.first(val) |> to_string |> String.to_integer
      _ ->
        IO.puts "tic_parsed returns nil"
        nil
    end
  end

  defp cat_parsed(body, xml_path) do
    doc = Floki.find(body, xml_path)
    {tag, attrs, val} = List.first(doc)
    case List.first(val) do
      nil ->
        0
      _ ->
        1
    end
  end

  defp dmoz_parsed(body, xml_path) do
    doc = Floki.find(body, xml_path)
    # doc |> IO.inspect
    # length(doc) |> IO.inspect
    length(doc)
  end
end
