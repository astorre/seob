defmodule Seob.Repo.Migrations.CreateCompetitor do
  use Ecto.Migration

  def change do
    create table(:competitors) do
      add :url, :string, null: false
      add :position, :integer
      add :task_id, references(:tasks, on_delete: :delete_all), null: false

      timestamps
    end
    create index(:competitors, [:task_id])

  end
end
