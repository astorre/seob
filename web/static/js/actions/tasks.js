import Constants              from '../constants';
import { push }               from 'react-router-redux';
import { httpGet, httpPost }  from '../utils';
import CurrentTaskActions     from './current_task';

const Actions = {
  fetchTasks: () => {
    return dispatch => {
      dispatch({ type: Constants.TASKS_FETCHING });

      httpGet('/api/v1/tasks')
      .then((data) => {
        dispatch({
          type: Constants.TASKS_RECEIVED,
          ownedTasks: data.owned_tasks
        });
      });
    };
  },

  showForm: (show) => {
    return dispatch => {
      dispatch({
        type: Constants.TASKS_SHOW_FORM,
        show: show,
      });
    };
  },

  create: (data) => {
    return dispatch => {
      httpPost('/api/v1/tasks', { task: data })
      .then((data) => {
        dispatch({
          type: Constants.TASKS_NEW_TASK_CREATED,
          task: data,
        });

        dispatch(push(`/tasks/${data.id}`));
      })
      .catch((error) => {
        error.response.json()
        .then((json) => {
          dispatch({
            type: Constants.TASKS_CREATE_ERROR,
            errors: json.errors,
          });
        });
      });
    };
  },
};

export default Actions;
