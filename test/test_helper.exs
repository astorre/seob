ExUnit.start

Mix.Task.run "ecto.create", ~w(-r Seob.Repo --quiet)
Mix.Task.run "ecto.migrate", ~w(-r Seob.Repo --quiet)
Ecto.Adapters.SQL.begin_test_transaction(Seob.Repo)

